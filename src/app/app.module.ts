import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FeatureModule } from 'src/feature/feature.module';

@Module({
  imports: [
    FeatureModule,
    MongooseModule.forRoot('mongodb://localhost:27017/moneylion'),
  ],
})
export class AppModule {}
