import { Test, TestingModule } from '@nestjs/testing';
import { STATUS_CODES } from 'http';
import { CreateFeatureDto } from '../dto/create-feature.dto';
import { GetFeatureDTO, GetFeatureReturnDTO } from '../dto/get-feature.dto';
import { FeatureController } from '../feature.controller';
import { FeatureService } from '../feature.service';

describe('FeatureController', () => {
  let controller: FeatureController;

  const featureServiceMock = {
    create: jest.fn(),
    findOne: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FeatureController],
      providers: [{ provide: FeatureService, useValue: featureServiceMock }],
    }).compile();

    controller = module.get<FeatureController>(FeatureController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create', () => {
    it('should return HTTP status 200 when status update or creation is succesful', async () => {
      const mockRequest: CreateFeatureDto = {
        email: 'hello@mail.com',
        featureName: 'read-user',
        enable: true,
      };
      featureServiceMock.create.mockImplementation(() => STATUS_CODES[200]);
      expect(await controller.create(mockRequest)).toBe(STATUS_CODES[200]);
      expect(featureServiceMock.create).toBeCalledWith(mockRequest);
    });

    it('should return HTTP status 304 when no changes is done', async () => {
      const mockRequest: CreateFeatureDto = {
        email: 'hello@mail.com',
        featureName: 'read-user',
        enable: true,
      };
      featureServiceMock.create.mockImplementation(() => STATUS_CODES[304]);
      expect(await controller.create(mockRequest)).toBe(STATUS_CODES[304]);
      expect(featureServiceMock.create).toBeCalledWith(mockRequest);
    });
  });

  describe('findOne', () => {
    it('should return canAccess: true when user can access the feature', async () => {
      const mockRequest: GetFeatureDTO = {
        email: 'hello@mail.com',
        featureName: 'read-user',
      };
      const mockResponse: GetFeatureReturnDTO = {
        canAccess: true,
      };
      featureServiceMock.findOne.mockImplementation(() => mockResponse);
      expect(await controller.findOne(mockRequest)).toBe(mockResponse);
      expect(featureServiceMock.findOne).toBeCalledWith(mockRequest);
    });

    it('should return canAccess: false when user cannot access the feature or not found', async () => {
      const mockRequest: GetFeatureDTO = {
        email: 'hello@mail.com',
        featureName: 'write-user',
      };
      const mockResponse: GetFeatureReturnDTO = {
        canAccess: false,
      };
      featureServiceMock.findOne.mockImplementation(() => mockResponse);
      expect(await controller.findOne(mockRequest)).toBe(mockResponse);
      expect(featureServiceMock.findOne).toBeCalledWith(mockRequest);
    });
  });
});
