import { HttpException } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { CreateFeatureDto } from '../dto/create-feature.dto';
import { GetFeatureDTO } from '../dto/get-feature.dto';
import { FeatureService } from '../feature.service';

describe('FeatureService', () => {
  let service: FeatureService;

  const featureModelMock = {
    findOne: jest.fn(),
    updateOne: jest.fn(),
    create: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FeatureService,
        {
          provide: getModelToken('Feature'),
          useValue: featureModelMock,
        },
      ],
    }).compile();

    service = module.get<FeatureService>(FeatureService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create the new entry', async () => {
      const createFeature: CreateFeatureDto = {
        email: 'hello@mail.com',
        featureName: 'read-user',
        enable: true,
      };
      featureModelMock.create.mockImplementation(() => createFeature);
      featureModelMock.findOne.mockImplementation(() => null);
      expect(await service.create(createFeature)).toBe(undefined);
      expect(featureModelMock.findOne).toBeCalledWith({
        email: createFeature.email,
        featureName: createFeature.featureName,
      });
      expect(featureModelMock.findOne).toBeCalledTimes(1);
      expect(featureModelMock.create).toBeCalledTimes(1);
      expect(featureModelMock.updateOne).toBeCalledTimes(0);
    });

    it('should update the existing entry', async () => {
      const createFeature: CreateFeatureDto = {
        email: 'hello@mail.com',
        featureName: 'read-user',
        enable: true,
      };
      const existingFeature: CreateFeatureDto = {
        email: 'hello@mail.com',
        featureName: 'read-user',
        enable: false,
      };
      featureModelMock.findOne.mockImplementation(() => existingFeature);
      expect(await service.create(createFeature)).toBe(undefined);
      expect(featureModelMock.findOne).toBeCalledWith({
        email: createFeature.email,
        featureName: createFeature.featureName,
      });
      expect(featureModelMock.findOne).toBeCalledTimes(1);
      expect(featureModelMock.create).toBeCalledTimes(0);
      expect(featureModelMock.updateOne).toBeCalledTimes(1);
    });

    it('should throw not updated exception when no change is done', async () => {
      const createFeature: CreateFeatureDto = {
        email: 'hello@mail.com',
        featureName: 'read-user',
        enable: true,
      };
      const existingFeature: CreateFeatureDto = {
        email: 'hello@mail.com',
        featureName: 'read-user',
        enable: true,
      };
      featureModelMock.findOne.mockImplementation(() => existingFeature);
      try {
        await service.create(createFeature);
        fail();
      } catch (e) {
        if (e instanceof HttpException) {
          const res = e.getResponse();
          expect(res).toEqual('update not succesful');
        } else {
          fail();
        }
      }
      expect(featureModelMock.findOne).toBeCalledWith({
        email: createFeature.email,
        featureName: createFeature.featureName,
      });
      expect(featureModelMock.findOne).toBeCalledTimes(1);
      expect(featureModelMock.updateOne).toBeCalledTimes(0);
    });
  });

  describe('findOne', () => {
    it('should return canAccess: true when user can access the feature', async () => {
      const existingFeature: CreateFeatureDto = {
        email: 'hello@mail.com',
        featureName: 'read-user',
        enable: true,
      };
      const getFeatureQuery: GetFeatureDTO = {
        email: 'hello@mail.com',
        featureName: 'read-user',
      };
      featureModelMock.findOne.mockImplementation(() => existingFeature);
      expect(await service.findOne(getFeatureQuery)).toStrictEqual({
        canAccess: true,
      });
    });

    it('should return canAccess: true when user can access the feature', async () => {
      const existingFeature: CreateFeatureDto = {
        email: 'hello@mail.com',
        featureName: 'read-user',
        enable: true,
      };
      const getFeatureQuery: GetFeatureDTO = {
        email: 'hello@mail.com',
        featureName: 'read-user',
      };
      featureModelMock.findOne.mockImplementation(() => existingFeature);
      expect(await service.findOne(getFeatureQuery)).toStrictEqual({
        canAccess: true,
      });
    });

    it('should return canAccess: false when user is not found', async () => {
      const getFeatureQuery: GetFeatureDTO = {
        email: 'hello@mail.com',
        featureName: 'read-user',
      };
      featureModelMock.findOne.mockImplementation(() => null);
      expect(await service.findOne(getFeatureQuery)).toStrictEqual({
        canAccess: false,
      });
    });
  });
});
