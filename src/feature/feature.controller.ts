import { FeatureService } from './feature.service';
import {
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetFeatureDTO, GetFeatureReturnDTO } from './dto/get-feature.dto';
import { CreateFeatureDto } from './dto/create-feature.dto';
import { Controller, Get, Post, Body, Query, HttpCode } from '@nestjs/common';

@ApiTags('feature')
@Controller('feature')
export class FeatureController {
  constructor(private readonly featureService: FeatureService) {}

  @Post()
  @HttpCode(200)
  @ApiOkResponse({ description: 'Update successful' })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiResponse({ status: 304, description: 'Not modified' })
  async create(@Body() createFeatureDto: CreateFeatureDto) {
    return this.featureService.create(createFeatureDto);
  }

  @Get()
  @ApiQuery({ name: 'featureName' })
  @ApiQuery({ name: 'email' })
  @ApiOkResponse({ description: 'Query successful', type: GetFeatureReturnDTO })
  @ApiBadRequestResponse({ description: 'Bad request' })
  async findOne(@Query() query: GetFeatureDTO) {
    return this.featureService.findOne(query);
  }
}
