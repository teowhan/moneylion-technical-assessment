import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type FeatureDocument = Feature & Document;

@Schema()
export class Feature {
  @Prop({ required: true })
  email: string;

  @Prop({ required: true })
  featureName: string;

  @Prop({ required: true })
  enable: boolean;
}

export const FeatureSchema = SchemaFactory.createForClass(Feature);
