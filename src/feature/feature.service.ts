import { Model } from 'mongoose';
import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { GetFeatureDTO } from './dto/get-feature.dto';
import { CreateFeatureDto } from './dto/create-feature.dto';
import { Feature, FeatureDocument } from './schema/feature.schema';

@Injectable()
export class FeatureService {
  constructor(
    @InjectModel(Feature.name) private featureModel: Model<FeatureDocument>,
  ) {}

  async create(createFeatureDto: CreateFeatureDto) {
    const existingDoc = await this.featureModel.findOne({
      email: createFeatureDto.email,
      featureName: createFeatureDto.featureName,
    });

    if (existingDoc) {
      if (existingDoc.enable === createFeatureDto.enable) {
        throw new HttpException('update not succesful', 304);
      }
      await this.featureModel.updateOne(
        { _id: existingDoc.id },
        { enable: createFeatureDto.enable },
      );
    } else {
      await this.featureModel.create(createFeatureDto);
    }
  }

  async findOne(query: GetFeatureDTO) {
    const result = await this.featureModel.findOne({
      email: query.email,
      featureName: query.featureName,
    });

    if (result) {
      if (result.enable) {
        return { canAccess: true };
      }
    }
    return { canAccess: false };
  }
}
