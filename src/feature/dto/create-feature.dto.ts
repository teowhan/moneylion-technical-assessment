import { IsBoolean, IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class CreateFeatureDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  featureName: string;

  @IsBoolean()
  @IsNotEmpty()
  enable: boolean;
}
