import { IsBoolean, IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class GetFeatureDTO {
  @IsEmail()
  @IsNotEmpty()
  email?: string;

  @IsString()
  @IsNotEmpty()
  featureName?: string;
}

export class GetFeatureReturnDTO {
  @IsBoolean()
  canAccess: boolean;
}
