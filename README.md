# MoneyLion technical assessment

## Tech Stack

- NestJS
- MongoDB
- Swagger documentation

## Installation

### Application

```bash
$ nvm install
$ npm install
```

### MongoDB

- install Docker
- pull MongoDB image with
  ```bash
  $ docker pull mongo
  ```
- start a docker container for mongo with port 27017
  ```bash
  docker run -d -p 27017:27017 --name example-mongo mongo:latest
  ```

## Running the app

```bash
$ npm run start
```

- go to http://localhost:3000/api to try out the API

## Test

```bash
$ npm run test
```

## Assumptions

- post endpoint can be used to add new feature permissions instead of just modifying the existing permissions
